import collections
import sys
import yt_dlp


with yt_dlp.YoutubeDL({'quiet': True}) as ydl:
	ie = ydl.get_info_extractor('YoutubeTab')
	for url in sys.argv[1:]:
		q = collections.deque()
		q.append(ie.extract(url))
		while q:
			e = q.popleft()
			if e['_type'] == 'playlist':
				q.extend(e['entries'])
			elif e['_type'] == 'url':
				print(e['id'])
